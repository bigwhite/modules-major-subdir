package bar

import "fmt"

func Bar() {
	fmt.Println("bar.Bar of module: bitbucket.org/bigwhite/modules-major-subdir")
}
